package wordcount;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Top10WordCount {

	public static void main(String[] args ) throws Exception {
		
		Path input = new File("data/lusiadas-utf8.txt").toPath();

		Files.lines(input, StandardCharsets.UTF_8)
			.parallel()
			.filter( line -> line.length() > 0 )
			.flatMap( line -> Stream.of( line.split(" |,")))
			.filter( word -> word.length() > 1 )
			.map( word -> word.toLowerCase())
			.collect( Collectors.toMap( word -> word, word -> 1, Integer::sum))
			.entrySet()
			.stream()
			.sorted( comparator )
			.limit(10)
			.forEach( e -> {
				System.err.println( e.getKey() + ":" + e.getValue() );
			});
	}
	
	static Comparator<Map.Entry<String,Integer>> comparator = new Comparator<Map.Entry<String,Integer>>() {
		@Override
		public int compare(Entry<String, Integer> a, Entry<String, Integer> b) {
			return Integer.compare( b.getValue(), a.getValue());
		}
	};
	
}