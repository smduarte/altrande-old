package wordcount;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WordCount {

	public static void main(String[] args ) throws Exception {
		
		Path input = new File("data/lusiadas-utf8.txt").toPath();

		Files.lines(input, StandardCharsets.UTF_8)
			.parallel()
			.filter( line -> line.length() > 0 )
			.flatMap( line -> Stream.of( line.split(" |,")))
			.filter( word -> word.length() > 1 )
			.map( word -> word.toLowerCase())
			.collect( Collectors.toMap( word -> word, word -> 1, Integer::sum))
			.forEach( (k,v) -> {
				System.err.println( k + ":" + v );
			});
	}
	
}